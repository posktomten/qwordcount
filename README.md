# qwordcount
A small terminal program to count and sort the occurrence of words (separated by spaces) in a text file.<br>
[Read more](https://gitlab.com/posktomten/qwordcount/-/wikis/home)<br>
