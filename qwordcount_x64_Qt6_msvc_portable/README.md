# qwordcount
A small terminal program to count and sort the occurrence of words (separated by spaces) in a text file.
run 'qwordcount.cmd -h'
Sourcecode https://gitlab.com/posktomten/qwordcount
