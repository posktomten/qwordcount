cmake_minimum_required(VERSION 3.16)
project(qwordcount VERSION 1.0 LANGUAGES CXX)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTORCC ON)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Qt6 REQUIRED COMPONENTS Core)

SET(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/../build-executable6)
# qt_standard_project_setup()
# Resources:

set(MY_RESOURCES
      ${CMAKE_CURRENT_SOURCE_DIR}/myres.qrc
)


 set(APP_ICON_RESOURCE_WINDOWS "${CMAKE_CURRENT_SOURCE_DIR}/myapp.rc")

add_executable(qwordcount
    main.cpp
    ${MY_RESOURCES}
    ${APP_ICON_RESOURCE_WINDOWS}

)
target_link_libraries(qwordcount PRIVATE
    Qt6::Core
)





#install(TARGETS qwordcount
#    BUNDLE DESTINATION .
#    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
#)

#qt_generate_deploy_app_script(
#    TARGET qwordcount
#    FILENAME_VARIABLE deploy_script
#    NO_UNSUPPORTED_PLATFORM_ERROR
#)
#install(SCRIPT ${deploy_script})
