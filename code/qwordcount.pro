#//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#//
#//          qwordcount
#//          Copyright (C) 2023 Ingemar Ceicer
#//          https://gitlab.com/posktomten/qwordcount
#//          programming@ceicer.com
#//
#//   This program is free software: you can redistribute it and/or modify
#//   it under the terms of the GNU General Public License version 3 as published by
#//   the Free Software Foundation.
#//
#//   This program is distributed in the hope that it will be useful,
#//   but WITHOUT ANY WARRANTY; without even the implied warranty of
#//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#//   GNU General Public License for more details.
#//
#// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*

QT -= gui

#CONFIG += c++17 console
CONFIG -= app_bundle
CONFIG += console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

TARGET=qwordcount

SOURCES += \
        main.cpp

# TRANSLATIONS += \
#    i18n/qwordcount_sv_SE.ts

RESOURCES += myres.qrc

equals(QT_MAJOR_VERSION, 5) {
DESTDIR="../build-executable5"
}

equals(QT_MAJOR_VERSION, 6) {
DESTDIR="../build-executable6"
}

#CONFIG += lrelease
#CONFIG += embed_translations

#win32:RC_ICONS += images/icon.ico

RC_FILE = myapp.rc

# Default rules for deployment.
#qnx: target.path = /tmp/$${TARGET}/bin
#else: unix:!android: target.path = /opt/$${TARGET}/bin
#!isEmpty(target.path): INSTALLS += target
