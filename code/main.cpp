//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          qwordcount
//          Copyright (C) 2023 Ingemar Ceicer
//          https://gitlab.com/posktomten/qwordcount
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QCoreApplication>
//#include <QLocale>
//#include <QTranslator>
#include <QDebug>
#include <QCommandLineOption>
#include <QCommandLineParser>
#include <QFileInfo>
#include <QRegularExpression>
#include <QSysInfo>
#include <QTextStream>
#include <QProcess>

#define VERSION "1.0.8"
#define EXECUTABLE_NAME "qwordcount"
#if defined(Q_OS_WIN) // WINDOWS
#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows.txt"
#endif

#if defined(Q_OS_LINUX) // LINUX


#define DOWNLOAD_SERVER "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/"
#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_linux.txt"


#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Ubuntu 24.04 LTS 64-bit, GLIBC 2.39"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 39

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.4 LTS 64-bit, GLIBC 2.35"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 35

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 27)
#if defined(Q_PROCESSOR_X86_64) // 64 bit
#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#elif defined(Q_PROCESSOR_X86_32) // 32 bit
#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#define ARG1 EXECUTABLE_NAME "-i386.AppImage"
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-i386.AppImage.zsync"
#endif // Q_PROCESSOR_
#endif // __GLIBC_MINOR__ == 27

#endif //  LINUX
using namespace std;

struct Par {
    int value;
    QString text;
};

bool fileInfo(QFileInfo *fi, QString executableName);
bool sortText(QString *in, bool sort_words, bool onlycount);
bool sort_by_value(const Par & lhs, const Par & rhs);
int main(int argc, char *argv[])
{
    const QString executablename(QString::fromLocal8Bit(argv[0]));
    QCoreApplication a(argc, argv);
    QCommandLineParser parser;
    /* LANGUAGE */
//    QTranslator translator;
//    const QStringList uiLanguages = QLocale::system().uiLanguages();
//    for(const QString &locale : uiLanguages) {
//        const QString baseName = ":/i18n/qwordcount_" + QLocale(locale).name() + ".qm";
//        const QString baseName = ":/i18n/qwordcount_sv_SE.qm";
//        if(translator.load(baseName)) {
//            a.installTranslator(&translator);
//            break;
//        }
//    }
    /* END LANGUAGE */
    parser.setOptionsAfterPositionalArgumentsMode(QCommandLineParser::ParseAsOptions);
    parser.setApplicationDescription(QCoreApplication::translate("main", "Counts the number of occurrences of words (separated by spaces) in a text file.\n(<the number of identical words>) <word>\nCopyright (C) 2023 Ingemar Ceicer.\nhttps://gitlab.com/posktomten/qwordcount\nprogramming@ceicer.com\nLicensed under the GNU General Public License version 3."));
    QCoreApplication::setApplicationName(QStringLiteral("qwordcount"));
    QCoreApplication::setApplicationVersion(QStringLiteral(VERSION));
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument(QCoreApplication::translate("main", "<input file>"), QCoreApplication::translate("main", "Full path and extension to the input file."));
    /*    */
    /*    */
#if defined(Q_OS_LINUX) // LINUX
    QCommandLineOption downloadOption(QStringList() << QStringLiteral("d") << QStringLiteral("download"), "The new AppImage has been created using zsync.\nOnly the changes are downloaded.");
    parser.addOption(downloadOption);
#endif
    /***/
    QCommandLineOption checkupdateOption(QStringList() << QStringLiteral("i") << QStringLiteral("checkforupdates"), "Check for updates.");
    parser.addOption(checkupdateOption);
    /***/
    QCommandLineOption aboutOption(QStringList() << "a" << "about", QCoreApplication::translate("main", "About qwordcount"));
    parser.addOption(aboutOption);
    /*    */
    QCommandLineOption onlycountOption(QStringList() << "c" << "count", QCoreApplication::translate("main", "Does not print the words. Counts the number of words and the number of unique words.\n<words> (<unique words>)"));
    parser.addOption(onlycountOption);
    /*    */
    QCommandLineOption historyOption(QStringList() << "history", QCoreApplication::translate("main", "Displays version history."));
    parser.addOption(historyOption);
    /*    */
    QCommandLineOption licenceOption(QStringList() << "l" << "license", QCoreApplication::translate("main", "Displays the license."));
    parser.addOption(licenceOption);
    /*      */
    QCommandLineOption displayOption(QStringList() << "w" << "word", QCoreApplication::translate("main", "Sort the words alphabetically. (Default is to sort by number of occurrences.)"));
    /*      */
    parser.addOption(displayOption);
    parser.process(a);
    /*      */
    const QStringList args = parser.positionalArguments();
    // source file is args.at(0)
#if defined(Q_OS_LINUX) // LINUX
    bool download = parser.isSet(downloadOption);
#endif
    bool checkupdate = parser.isSet(checkupdateOption);
    bool about = parser.isSet(aboutOption);
    bool sort_words = parser.isSet(displayOption);
    bool onlycount = parser.isSet(onlycountOption);
    bool license = parser.isSet(licenceOption);
    bool history = parser.isSet(historyOption);
    QString inputFileName;
#if defined(Q_OS_LINUX) //LINUX

    if(download) {
        QString path = QCoreApplication::applicationDirPath() + "/downloadandremove";
        QStringList ARG;
        ARG << ARG1 << ARG2 << "en_US" << VERSION;
        QProcess *process  = new QProcess(nullptr);
        process->startDetached(path, ARG);
        process->deleteLater();
        exit(0);
    }
    /***/
    else if(checkupdate) {
#endif
#if defined(Q_OS_WIN) // WINDOWS

        if(checkupdate) {
#endif
#if defined(Q_OS_WIN) // WINDOWS
            QString path = QCoreApplication::applicationDirPath() + "/checkforupdatescli.exe";
#endif
#if defined Q_OS_LINUX
            QString path = QCoreApplication::applicationDirPath() + "/checkforupdatescli";
#endif
            QStringList ARG;
            ARG  << VERSION_PATH  << VERSION;
            QProcess *process  = new QProcess;
            process->startDetached(path, ARG);
            process->deleteLater();
            exit(0);
        }
        /***/
        else  if(about) {
            QTextStream(stdout) << QCoreApplication::translate("main", "Compiled on: ") << QSysInfo::kernelType() + ' ' + QSysInfo::prettyProductName() + ' ' + QSysInfo::currentCpuArchitecture() + '\n';
            QTextStream(stdout) << QCoreApplication::translate("main", "Qt version: ") << QT_VERSION_STR << '\n';
            exit(0);
        } else if(license) {
            QFile fil(QStringLiteral(":/txt/LICENSE"));
            QString *intext = new QString;
            intext->clear();

            if(Q_LIKELY(fil.open(QFile::ReadOnly | QFile::Text))) {
                QTextStream in(&fil);
                intext->append(in.readAll());
            }

            QTextStream(stdout) << *intext;
            exit(0);
        } else if(history) {
            QFile fil(QStringLiteral(":/txt/CHANGELOG"));
            QString *intext = new QString;
            intext->clear();

            if(Q_LIKELY(fil.open(QFile::ReadOnly | QFile::Text))) {
                QTextStream in(&fil);
                intext->append(in.readAll());
            }

            QTextStream(stdout) << *intext;
            exit(0);
        } else if(args.size() < 1) {
            QTextStream(stdout) << QCoreApplication::translate("main", "NO VALID INPUT FILE\ntype: ");
            QTextStream(stdout) << "\""  << executablename << " -h\"";
            QTextStream(stdout) << QCoreApplication::translate("main", " for help.\n");
            exit(0);
        } else {
            inputFileName = args.at(0);
        }

        QString executableName = executablename;
        QFileInfo *fi  = new QFileInfo(inputFileName);

        if(!fileInfo(fi, executableName)) {
            exit(0);
        }

        QFile fil(inputFileName);
        QString *intext = new QString;
        intext->clear();

        if(fil.open(QFile::ReadOnly | QFile::Text)) {
            QTextStream in(&fil);
            intext->append(in.readAll());
        }

        if(sortText(intext, sort_words, onlycount)) {
            exit(0);
        }

        return a.exec();
    }

    bool fileInfo(QFileInfo * fi, QString executableName) {
        if(!fi->exists()) {
            QTextStream(stdout) << QCoreApplication::translate("main", "THE INPUT FILE COULD NOT BE FOUND\ntype: ");
            QTextStream(stdout) << "\""  << executableName << " -h\"";
            QTextStream(stdout) << QCoreApplication::translate("main", " for help.\n");
            return false;
        } else if(!fi->isReadable()) {
            QTextStream(stdout) << QCoreApplication::translate("main", "THE INPUT FILE IS NOT READABLE\ntype: ");
            QTextStream(stdout) << "\""  << executableName << " -h\"";
            QTextStream(stdout) << QCoreApplication::translate("main", " for help.\n");
            return false;
        } else {
            return true;
        }
    }

    bool sortText(QString * in, bool sort_words, bool onlycount) {
        in->replace("\n", " ");
//    qDebug() << *in;
        *in = in->trimmed();
        QStringList list2 = in->split(" ");
        QStringList list;

        for(int i = 0; i < list2.count(); i++) {
            list.append(list2.at(i).trimmed());
        }

        static QRegularExpression re("[,\\s]+");

        for(int i = 0; i < list.count(); i++) {
            QRegularExpressionMatch match = re.match(list.at(i));

            if(list.at(i).isEmpty()) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                list.removeAt(i);
#endif
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
                list.remove(i);
#endif
            }

            if(match.hasMatch()) {
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
                list.removeAt(i);
#endif
#if QT_VERSION >= QT_VERSION_CHECK(6, 0, 0)
                list.remove(i);
#endif
            }
        }

        QMap<QString, int> countOfStrings;

        for(int i = 0; i < list.count(); i++) {
            countOfStrings[list[i]]++;
        }

        if(onlycount) {
            QTextStream(stdout) << list.count();
            list.removeDuplicates();
            QTextStream(stdout) << " (" << list.count() << ")\n";
            exit(0);
        }

        list.removeDuplicates();
        list.sort();
        vector<Par> par;
        Par p;

        for(int i = 0; i < list.count(); i++) {
            if(!list[i].isEmpty()) {
                p.text = list[i];
                p.value = countOfStrings[list[i]];
                par.push_back(p);

                if(sort_words) {
                    QTextStream(stdout) << list[i].toUtf8() << " (" << ((countOfStrings[list[i]]++)) << ")\n";
                }
            }
        }

        if(!sort_words) {
            sort(par.begin(), par.end(), sort_by_value);

            for(const Par &p : par) {
                QTextStream(stdout) << "(" << p.value << ")" << " " << p.text.toUtf8() << "\n";
            }
        }

        return true;
    }

    bool sort_by_value(const Par & lhs, const Par & rhs) {
        return lhs.value > rhs.value;
    }
