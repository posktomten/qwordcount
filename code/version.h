
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             1,0,8,0
#define VER_FILEVERSION_STR         "1.0.8.0\0"

#define VER_PRODUCTVERSION          1,0,8,0
#define VER_PRODUCTVERSION_STR      "1.0.8\0"

#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "Counts the number of words in a text."
#define VER_INTERNALNAME_STR        "qwordcount"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2023 - 2024 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "qwordcount.exe"
#define VER_PRODUCTNAME_STR         "qwordcount"

#define VER_COMPANYDOMAIN_STR       "https://gitlab.com/posktomten/qwordcount"

#endif // VERSION_H

