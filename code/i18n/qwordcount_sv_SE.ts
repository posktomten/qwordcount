<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>main</name>
    <message>
        <location filename="../main.cpp" line="60"/>
        <source>&lt;input file&gt;</source>
        <translation>&lt;indatafil&gt;</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="63"/>
        <source>Does not print the words. Counts the number of words and the number of unique words.
&lt;words&gt; (&lt;unique words&gt;)</source>
        <translation>Skriver inte ut orden. Räknar antalet ord och antalet unika ord.
&lt;ord&gt; (&lt;unika ord&gt;)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>Counts the number of occurrences of words (separated by spaces) in a text file.
(&lt;the number of identical words&gt;) &lt;word&gt;
Copyright (C) 2023 Ingemar Ceicer.
https://gitlab.com/posktomten/qwordcount
programming@ceicer.com
Licensed under the GNU General Public License version 3.</source>
        <translation>Räknar antalet förekomster av ord (avgränsade med mellanslag) i en textfil.
(&lt;antalet identiska ord&gt;) &lt;ord&gt;
Copyright (C) 2023 Ingemar Ceicer.
https://gitlab.com/posktomten/qwordcount
programming@ceicer.com
Licensierad med GNU General Public License version 3.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="60"/>
        <source>Full path and extension to the input file.</source>
        <translation>Fullständig sökväg till indatafilen, inklusive filändelse.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="66"/>
        <source>Displays version history.</source>
        <translation>Visar versionshistorik.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="69"/>
        <source>Displays the license.</source>
        <translation>Visar licensen.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="72"/>
        <source>Sort the words alphabetically. (Default is to sort by number of occurrences.)</source>
        <translation>Sortera orden i alfabetisk ordning. (Standard är att sortera efter antal förekomster.)</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="114"/>
        <source>NO VALID INPUT FILE
type: </source>
        <translation>INGEN GILTIG INDATAFIL
skriv: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="116"/>
        <location filename="../main.cpp" line="150"/>
        <location filename="../main.cpp" line="155"/>
        <source> for help.
</source>
        <translation> för att få hjälp.
</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="148"/>
        <source>THE INPUT FILE COULD NOT BE FOUND
type: </source>
        <translation>INDATAFILEN KUNDE INTE HITTAS
skriv: </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="153"/>
        <source>THE INPUT FILE IS NOT READABLE
type: </source>
        <translation>INDATAFILEN ÄR INTE LÄSBAR
skriv: </translation>
    </message>
</context>
</TS>
